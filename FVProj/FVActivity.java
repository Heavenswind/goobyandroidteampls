package as1.com.fvpack;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class FVActivity extends Activity {
	
	private EditText name, assignment1, assignment2, exam1, exam2;
	private Button add, clear, compute;
	private StudentList list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fv);
		
		// Edit Texts
		this.name = (EditText)findViewById(R.id.name);
		this.assignment1 = (EditText)findViewById(R.id.assignment1);
		this.assignment2 = (EditText)findViewById(R.id.assignment2);
		this.exam1 = (EditText)findViewById(R.id.exam1);
		this.exam2 = (EditText)findViewById(R.id.exam2);
		
		//Buttons
		this.add = (Button)findViewById(R.id.add);
		this.add.setOnClickListener(new PerformanceActivityClick());
		
		this.clear = (Button)findViewById(R.id.clear);
		this.add.setOnClickListener(new PerformanceActivityClick());
		
		this.compute = (Button)findViewById(R.id.compute);
		this.add.setOnClickListener(new PerformanceActivityClick());
		
		//Array List containing students as data structure
		this.list = new StudentList();
	}
	
	protected class PerformanceActivityClick implements OnClickListener{

		@Override
		public void onClick(View v) {
			
			
		}
		
	}
}
	
