/* Assignment 1
 * @Author: Camilo Vides & Fabian Vergara
 * Date   : February 10, 2014
 * FVActivity class
 */

package as1.com.fvpack;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class FVActivity extends Activity {

	EditText name, assignment1, assignment2, exam1, exam2;
	Button add, clear, compute;
	ProgressBar progressBar;

	// static StudentList list;
	ArrayList<Student> list;
	private static final String EMPTY_STRING = "";
	private int invalidStudents = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fv);

		//progressBar = (ProgressBar) findViewById(R.id.progressBar1);

		// Edit Texts
		name = (EditText) findViewById(R.id.name);
		assignment1 = (EditText) findViewById(R.id.assignment1);
		assignment2 = (EditText) findViewById(R.id.assignment2);
		exam1 = (EditText) findViewById(R.id.exam1);
		exam2 = (EditText) findViewById(R.id.exam2);
		
		

		// Buttons
		add = (Button) findViewById(R.id.add);
		add.setOnClickListener(new PerformanceActivityClick());

		clear = (Button) findViewById(R.id.clear);
		clear.setOnClickListener(new PerformanceActivityClick());

		compute = (Button) findViewById(R.id.compute);
		compute.setOnClickListener(new PerformanceActivityClick());

		// Array List containing students as data structure
		this.list = new ArrayList<Student>();
	}

	protected class PerformanceActivityClick implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.add) {

				// It creates a new student with data provided
				Student newStudent = new Student(name.getText().toString(),
						Double.parseDouble(assignment1.getText().toString()),
						Double.parseDouble(assignment2.getText().toString()),
						Double.parseDouble(exam1.getText().toString()),
						Double.parseDouble(exam2.getText().toString()));
				// And it adds the student to our student list contained in an
				
				
				if ( !newStudent.isValid()){
					invalidStudents++;
					Context context = getApplicationContext();	
					Toast toast = Toast.makeText(context, newStudent.getName()+ " couldn't be added. He had an invalid grade", Toast.LENGTH_SHORT);
					toast.show();
					
				}else{
					list.add(newStudent);
					//Show a message displaying that the student was added
					Context context = getApplicationContext();	
					Toast toast = Toast.makeText(context, newStudent.getName()+ " has been added", Toast.LENGTH_SHORT);
					toast.show();
				}
						
				//Then clear for the input for a new student.
				clearInput();
				
			} else if (v.getId() == R.id.clear) {
				clearInput();

			} else {
				//progressBar.setVisibility(1);

				for (int i = 0; i < list.size(); i++) {
					list.get(i).setFinalNumericGrade();
					list.get(i).setFinalGrade();
				}
				//progressBar.setVisibility(0);
				
				Intent intent = new Intent(v.getContext(), ReportActivity.class);
				intent.putParcelableArrayListExtra("STUDENT_LIST", list);
				intent.putExtra("INVALID_STUDENTS", invalidStudents);
				startActivityForResult(intent, 0);

			}

		}
		//Method to clear all the input in the UI
		private void clearInput() { 	
			name.setText(EMPTY_STRING);
			assignment1.setText(EMPTY_STRING);
			assignment2.setText(EMPTY_STRING);
			exam1.setText(EMPTY_STRING);
			exam2.setText(EMPTY_STRING);
		}
	}
}
