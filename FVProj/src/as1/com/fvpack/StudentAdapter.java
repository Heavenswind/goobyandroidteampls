/* Assignment 1
 * @Author: Camilo Vides & Fabian Vergara
 * Date   : February 10, 2014
 * StudentAdapter Class
 */

package as1.com.fvpack;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StudentAdapter extends BaseAdapter {

	// Setting up the data members for the adapter
	private Activity activity;
	private ArrayList<Student> data;
	private static LayoutInflater inflater = null;
	public Resources res;
	Student tempValue = null;
	int i = 0;

	// Using the implemented constructor from the BaseAdapter class
	public StudentAdapter(Activity a, ArrayList<Student> d, Resources resLocal) {
		activity = a;
		data = d;
		res = resLocal;

		/*********** Layout inflator to call external xml layout () ***********/
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	// What is the size of Passed Arraylist Size
	public int getCount() {
		if (data.size() <= 0)
			return 1;
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	//Create a holder Class to contain inflated xml file elements
	public static class ViewHolder {

		public TextView name;
		public TextView score;
		public TextView grade;

	}

	//Depends upon data size called for each row , Create each ListView row
	public View getView(int position, View convertView, ViewGroup parent) {

		View vi = convertView;
		ViewHolder holder;

		if (convertView == null) {

			/****** Inflate tabitem.xml file for each row ( Defined below ) *******/
			vi = inflater.inflate(R.layout.student_list, null);

			/****** View Holder Object to contain tabitem.xml file elements ******/
			holder = new ViewHolder();
			holder.name = (TextView) vi.findViewById(R.id.name);
			holder.score = (TextView) vi.findViewById(R.id.score);
			holder.grade = (TextView) vi.findViewById(R.id.grade);

			/************ Set holder with LayoutInflater ************/
			vi.setTag(holder);
		} else
			holder = (ViewHolder) vi.getTag();

		if (data.size() <= 0) {
			holder.name.setText("No Data");

		} else {
			/***** Get each Model object from Arraylist ********/
			tempValue = null;
			tempValue = (Student) data.get(position);

			/************ Set Model values in Holder elements ***********/
			holder.name.setText(tempValue.getName());
			holder.score.setText(Integer.toString((int) Math.round(tempValue
					.getFinalNumericGrade())) + "%");
			holder.grade.setText(tempValue.getFinalGrade());

		}
		return vi;
	}
}