/* Assignment 1
 * @Author: Camilo Vides & Fabian Vergara
 * Date   : February 10, 2014
 * ReportActivity class
 */

package as1.com.fvpack;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class ReportActivity extends Activity {

	ListView list;
	StudentAdapter adapter;
	public ArrayList<Student> students;
	TextView numPassing, groupAverage, invalidScores, bestStudent, copyRight;
	int invalidStudents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_summary);

		// Get The students arrayList to send it to the adapter.
		students = getIntent().getExtras().getParcelableArrayList(
				"STUDENT_LIST");
		invalidStudents = getIntent().getIntExtra("INVALID_STUDENTS", 0);

		// ListView update
		ListView studentList = (ListView) findViewById(R.id.student_listview);
		studentList.setTextFilterEnabled(true);

		// Setting up the adapter to inflate the ListView
		Resources res = getResources();
		StudentAdapter studentAdapter = new StudentAdapter(this, students, res);
		studentList.setAdapter(studentAdapter);

		// ----Summary------
		numPassing = (TextView) findViewById(R.id.numPassing);
		groupAverage = (TextView) findViewById(R.id.groupAverage);
		invalidScores = (TextView) findViewById(R.id.invalidScores);
		bestStudent = (TextView) findViewById(R.id.bestStudent);
		copyRight = (TextView) findViewById(R.id.copyRight);

		// ----Showing the results-----------------------
		numPassing.setText(getNumPassing(students));
		groupAverage.setText(getGroupAverage(students));
		invalidScores.setText(getInvalidScores());
		bestStudent.setText(getBestStudent(students));
		copyRight.setText(recursiveCopyright("Copyright: This script was written by Fabian Vergara & Camilo Vides"));
	}

	//Number of Passing grades, looks through two arrays to sort the best students
	private String getBestStudent(ArrayList<Student> students2) {
		double best = Integer.MIN_VALUE;
		String topStudents = "";
		ArrayList<Student> goodStudents = new ArrayList<Student>();
		for (int i = 0; i < students.size(); i++) {
			if (students.get(i).getFinalNumericGrade() >= best) {
				best = students.get(i).getFinalNumericGrade();
				goodStudents.add(students.get(i));
			}
		}
		for (int i = 0; i < goodStudents.size(); i++) {
			if (goodStudents.get(i).getFinalNumericGrade() == best) {
				topStudents += goodStudents.get(i).getName() + "\n";
			}
		}
		return topStudents;
	}
	
	//Method that will output the number of invalid scores
	private String getInvalidScores() {
		return Integer.toString(invalidStudents);
	}
	
	//Get all the grades of the students, add them and return the average
	private String getGroupAverage(ArrayList<Student> students) {
		double accumulate = 0;
		for (int i = 0; i < students.size(); i++) {
			accumulate += students.get(i).getFinalNumericGrade();
		}
		return Integer.toString((int) accumulate / students.size());
	}

	//Looks through the Number of passing grades and outputs them
	private String getNumPassing(ArrayList<Student> students2) {
		int numPassing = students.size();
		for (int i = 0; i < students.size(); i++)
			numPassing -= (!students.get(i).isPassing()) ? 1 : 0;
		return Integer.toString(numPassing);
	}

	//Recursively output the copyright message
	private String recursiveCopyright(String copyRight) {
		return (copyRight.length() == 0) ? "" : copyRight.charAt(0)
				+ recursiveCopyright(copyRight.substring(1));
	}

}