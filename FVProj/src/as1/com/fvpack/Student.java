/* Assignment 1
 * @Author: Camilo Vides & Fabian Vergara
 * Date   : February 10, 2014
 * Student Class
 */

package as1.com.fvpack;

import java.text.DecimalFormat;

import android.os.Parcel;
import android.os.Parcelable;

public class Student implements Parcelable {
	
	private static int id;
	private String name;
	private double assignment1, assignment2, exam1, exam2, finalGrade;
	private String grade;
	
	
	public Student(String name, double assignment1, double assignment2, double exam1, double exam2){
		this.id += 1;
		this.name = name;
		this.assignment1 = assignment1;
		this.assignment2 = assignment2;
		this.exam1 = exam1;
		this.exam2 = exam2;
		this.finalGrade = 0;

	}
	
	private Student(Parcel in) {
		readFromParcel(in);
	}

	/*/!\ Setters /!\ */
	public void setFinalNumericGrade(){
		setFinalNumericGrade( (this.assignment1 * 0.10) 
							+ (this.assignment2 * 0.20) 
							+ (this.exam1 * 0.30) 
							+ (this.exam2 * 0.40)     );
	}
	
	private void setFinalNumericGrade(double finalGrade) {
		DecimalFormat d = new DecimalFormat("##.00");
		this.finalGrade = Double.parseDouble(d.format(finalGrade));
	}
	
	public void setFinalGrade(){
		if ( this.finalGrade >= 90){
			setGrade("A");
		}else if ( this.finalGrade >= 80){
			setGrade("B");
		}else if ( this.finalGrade >= 70){
			setGrade("C");
		}else if ( this.finalGrade >= 60){
			setGrade("D");
		}else{
			setGrade("F");
		}
	}
	
	private void setGrade(String grade) {
		this.grade = grade;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public boolean isValid(){
		return !( this.assignment1 > 100 || this.assignment2 > 100 
		  || this.exam1       > 100 || this.exam2       > 100);
	}
	
	/*/!\ Getters /!\ */
	public String getName() {
		return this.name;
	}

	public double getFinalNumericGrade() {
		return this.finalGrade;
	}

	public String getFinalGrade() {
		return this.grade;
	}
	
	
	private void readFromParcel(Parcel in) {
		name = in.readString();
		assignment1 = in.readDouble();
		assignment2 = in.readDouble();
		exam1 = in.readDouble();
		exam2 = in.readDouble();
		finalGrade = in.readDouble();
		grade = in.readString();
		
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(name);
		out.writeDouble(assignment1);
		out.writeDouble(assignment2);
		out.writeDouble(exam1);
		out.writeDouble(exam2);
		out.writeDouble(finalGrade);
		out.writeString(grade + "");

	}
	public static final Parcelable.Creator<Student> CREATOR = new Parcelable.Creator<Student>() {
	    public Student createFromParcel(Parcel in) {
	        return new Student(in);
	    }

		@Override
		public Student[] newArray(int size) {
			return new Student[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isPassing() {
		char[] letter = this.grade.toCharArray();
		switch(letter[0]){
		case 'A':
		case 'B':
		case 'C':
		case 'D':
			return true;
		case 'F' : return false;
		default  : return false;
		}
	}
}
